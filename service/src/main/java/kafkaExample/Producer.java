package kafkaExample;

import static java.util.concurrent.TimeUnit.*;
import java.time.Instant;
import java.util.concurrent.*;
import java.util.Properties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class Producer {

    private final static String TOPIC = "input";
    private final static String BOOTSTRAP_SERVERS = "192.168.2.11:9092";
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public void main(String[] args) {
        final Runnable sender = new Runnable() {
            public void run()
            {
                long time = System.currentTimeMillis();
                Properties props = new Properties();
                props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
                props.put(ProducerConfig.ACKS_CONFIG, "all");
                props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
                props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
                KafkaProducer<Long, String> producer = new KafkaProducer<Long, String>(props);

                try {
                    ProducerRecord<Long, String> data;
                    Long t = Instant.now().toEpochMilli();
                    data = new ProducerRecord<Long, String>(TOPIC, t, Long.toString(t));
                    RecordMetadata metadata = producer.send(data).get();
                    //System.out.printf(t + "\n");
                } catch (Exception e)
                {
                    System.out.printf("Exception!\n");
                    e.printStackTrace(System.out);
                } finally {
                    producer.flush();
                    producer.close();
                }
            }
        };
        final ScheduledFuture<?> senderHandle = scheduler.scheduleAtFixedRate(sender, 1, 1, SECONDS);
        //scheduler.schedule(new Runnable() { public void run() { senderHandle.cancel(true); } }, 1000, SECONDS);
    }
}
