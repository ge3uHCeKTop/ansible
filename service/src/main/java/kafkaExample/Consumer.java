package kafkaExample;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.text.DateFormat; 
import java.text.SimpleDateFormat; 
import java.util.Arrays;
import java.util.Date; 
import java.util.Properties;

public class Consumer {
    private final static String TOPIC = "input";
    private final static String TOPICOUT = "output";
    private final static String BOOTSTRAP_SERVERS = "192.168.2.11:9092";

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "input-consumer-group");
        KafkaConsumer consumer = new KafkaConsumer(props);
        consumer.subscribe(Arrays.asList(TOPIC));
        while (true) {
            ConsumerRecords<Long, String> recs = consumer.poll(100);
            if (recs.count() == 0) {
            } else {
                for (ConsumerRecord<Long, String> rec : recs) {
                    Properties prdprops = new Properties();
                    prdprops.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
                    prdprops.put(ProducerConfig.ACKS_CONFIG, "all");
                    prdprops.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
                    prdprops.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
                    KafkaProducer<Long, String> producer = new KafkaProducer<Long, String>(prdprops);

                    String val = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(new Date(rec.key()));
                    try {
                        ProducerRecord<Long, String> data;
                        data = new ProducerRecord<Long, String>(TOPICOUT, rec.key(), val);
                        RecordMetadata metadata = producer.send(data).get();
                    } catch (Exception e) {
                        System.out.printf("Exception!\n");
                        e.printStackTrace(System.out);
                    } finally {
                        producer.flush();
                        producer.close();
                    }
                }
            }
        }
    }
}
