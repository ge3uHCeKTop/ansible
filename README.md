# HowTo

To start test infrastructure clone this repository, go to `vagrant` folder and in each of `cos` folders run `vagrant up --provision`
All 3 VMs will be available via ssh using `cos.ppk`.

* cos1 - 192.168.2.11:22

* cos2 - 192.168.2.12:22

* cos3 - 192.168.2.13:22

After successfull deployment metrics should be available at http://192.168.2.13:3000/ (login: admin; password: admin)
